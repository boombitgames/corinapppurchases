//
//  CORInAppPurchases.swift
//  CORInAppPurchases
//
//  Created by Siema on 10/11/2020.
//

import Foundation
import StoreKit

import CORCommon

public class CORInAppPurchases : NSObject {
    @objc public static let shared = CORInAppPurchases()
    
    let jsonEncoder = JSONEncoder()
    let currencyFormatter = NumberFormatter()
    
    private let receiptRefreshDelegate = ReceiptRefreshDelegate()
    
    var products = [String: Product]()
    var transactions = [String: Transaction]()
    var pendingTransactions = [SKPaymentTransaction]()
    
    var productsReady = false
    var refreshingReceipt = false
    
    private override init() {
        super.init()
        
        jsonEncoder.dateEncodingStrategy = .iso8601
        
        currencyFormatter.formatterBehavior = .behavior10_4
        currencyFormatter.numberStyle = .currency
        
        SKPaymentQueue.default().add(self)
        
        NSLog("InAppPurchases :: CORInAppPurchases.init :: Initialized")
    }
    
    @objc public func requestProducts(_ identifierString: String?) {
        guard let identifiers = identifierString?.components(separatedBy: ",") else { return }
        
        let request = SKProductsRequest(productIdentifiers: Set(identifiers))
        request.delegate = self
        request.start()
    }
    
    @objc public func restoreCompletedTransactions() {
        UnityProxyQueryPurchasesStarted()
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    @objc public func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    @objc public func purchase(_ productIdentifier: String) {
        guard canMakePayments(), let product = products[productIdentifier] else {
            UnityProxyPurchasesUpdated(Transactions(Transaction(productIdentifier: productIdentifier,
                                                                errorCode: Int(SKError.paymentNotAllowed.rawValue),
                                                                errorDescription: "Payments unavailable")).jsonString)
            return
        }
        
        SKPaymentQueue.default().add(SKPayment(product: product.storeKitProduct))
    }
    
    @objc public func finishTransaction(_ transactionIdentifier: String) {
        guard let skTransaction = transactions[transactionIdentifier]?.storeKitTransaction else { return }
        
        SKPaymentQueue.default().finishTransaction(skTransaction)
    }
    
    @objc public func refreshReceipt() {
        if (refreshingReceipt) { return }
        
        let request = SKReceiptRefreshRequest()
        request.delegate = receiptRefreshDelegate
        request.start()
        
        refreshingReceipt = true
    }
    
    public func receiptRefreshFinished() {
        refreshingReceipt = false
    }
    
    @objc public func getReceiptPath() -> String {
        return Bundle.main.appStoreReceiptURL?.path ?? ""
    }
    
    @objc public func getReceipt() -> String {
        do {
            guard let url = Bundle.main.appStoreReceiptURL else { throw CORError("AppStore receipt URL is nil") }
            let data = try Data(contentsOf: url)
            return data.base64EncodedString(options: .init(rawValue: 0))
        } catch {
            NSLog("InAppPurchases :: CORInAppPurchases.getReceipt :: Error when fetching receipt: \(error)")
            return ""
        }
    }
    
    @objc public func getStoreCountryCode() -> String? {
        return SKPaymentQueue.default().storefront?.countryCode
    }
    
    func getProductsJson() -> String {
        do {
            let data = try jsonEncoder.encode(Array(products.values))
            if let jsonString = String(data: data, encoding: .utf8) {
                return jsonString
            } else {
                throw CORError("Couldn't convert JSON-encoded product data to String")
            }
        } catch {
            NSLog("InAppPurchases :: CORInAppPurchases.getProducts :: Error encoding JSON: \(error)")
            return ""
        }
    }
}
