//
//  Product.swift
//  CORInAppPurchases
//
//  Created by Siema on 20/11/2020.
//

import Foundation
import StoreKit

class Product: Encodable {
    let storeKitProduct: SKProduct
    let productIdentifier: String
    let localizedTitle: String
    let localizedDescription: String
    let price: Decimal
    let currencySymbol: String
    let currencyCode: String
    let formattedPrice: String
    let countryCode: String
    
    private enum CodingKeys: CodingKey {
        case productIdentifier,
             localizedTitle,
             localizedDescription,
             price,
             currencySymbol,
             currencyCode,
             formattedPrice,
             countryCode
    }
    
    init(_ storeKitProduct: SKProduct) {
        self.storeKitProduct = storeKitProduct
        productIdentifier = storeKitProduct.productIdentifier
        localizedTitle = storeKitProduct.localizedTitle
        localizedDescription = storeKitProduct.localizedDescription
        price = storeKitProduct.price as Decimal
        currencySymbol = storeKitProduct.priceLocale.currencySymbol ?? "$"
        currencyCode = storeKitProduct.priceLocale.currencyCode ?? "USD"
        CORInAppPurchases.shared.currencyFormatter.locale = storeKitProduct.priceLocale
        formattedPrice = CORInAppPurchases.shared.currencyFormatter.string(from: storeKitProduct.price) ?? "$0.00"
        countryCode = storeKitProduct.priceLocale.regionCode ?? "US"
    }
}
