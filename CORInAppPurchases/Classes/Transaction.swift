//
//  Transaction.swift
//  CORInAppPurchases
//
//  Created by Siema on 20/11/2020.
//

import Foundation
import StoreKit

import CORCommon

class Transaction: Encodable {
    let storeKitTransaction: SKPaymentTransaction?
    let transactionState: String
    let transactionIdentifier: String?
    let productIdentifier: String
    let date: Date?
    let quantity: Int
    let orginalTransaction: String?
    let storeKitErrorCode: Int
    let storeKitErrorDescription: String?
    
    private enum CodingKeys: CodingKey {
        case transactionState,
             transactionIdentifier,
             productIdentifier,
             date,
             quantity,
             orginalTransaction,
             storeKitErrorCode,
             storeKitErrorDescription
    }
    
    init(_ storeKitTransaction: SKPaymentTransaction) {
        self.storeKitTransaction = storeKitTransaction
        transactionState = Transaction.getStateString(storeKitTransaction.transactionState)
        transactionIdentifier = storeKitTransaction.transactionIdentifier
        productIdentifier = storeKitTransaction.payment.productIdentifier
        date = (storeKitTransaction.original != nil) ? storeKitTransaction.original?.transactionDate : storeKitTransaction.transactionDate
        quantity = storeKitTransaction.payment.quantity
        orginalTransaction = storeKitTransaction.original?.transactionIdentifier
        storeKitErrorCode = Int(((storeKitTransaction.error as? SKError)?.code ?? .unknown).rawValue)
        storeKitErrorDescription = storeKitTransaction.error?.localizedDescription
    }
    
    init(productIdentifier productId: String, errorCode: Int, errorDescription: String?) {
        storeKitTransaction = nil
        transactionState = Transaction.getStateString(.failed)
        transactionIdentifier = nil
        productIdentifier = productId
        date = nil
        quantity = 0
        orginalTransaction = nil
        storeKitErrorCode = errorCode
        storeKitErrorDescription = errorDescription
    }
    
    var jsonString: String {
        do {
            let data = try CORInAppPurchases.shared.jsonEncoder.encode(self)
            if let jsonString = String(data: data, encoding: .utf8) { return jsonString }
            else { throw CORError("Couldn't convert encoded data to JSON String") }
        } catch {
            NSLog("InAppPurchases :: Transaction.jsonString :: Error encoding JSON: \(error)")
            return ""
        }
    }
    
    static func getStateString(_ transactionState: SKPaymentTransactionState) -> String {
        switch transactionState {
            case .purchasing:
                return "Purchasing"
            case .purchased:
                return "Purchased"
            case .restored:
                return "Restored"
            case .deferred:
                return "Deferred"
            default:
                return "Failed"
        }
    }
}
