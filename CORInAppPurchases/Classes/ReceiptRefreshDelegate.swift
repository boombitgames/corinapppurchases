//
//  ReceiptRefreshDelegate.swift
//  CORInAppPurchases
//
//  Created by Siema on 28/10/2020.
//

import Foundation
import StoreKit

import CORCommon

class ReceiptRefreshDelegate : NSObject, SKRequestDelegate {
    public func requestDidFinish(_ request: SKRequest) {
        NSLog("WOLOLO requestDidFinish")
        CORInAppPurchases.shared.receiptRefreshFinished()
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        NSLog("WOLOLO request:didFailWithError")
        CORInAppPurchases.shared.receiptRefreshFinished()
    }
}
