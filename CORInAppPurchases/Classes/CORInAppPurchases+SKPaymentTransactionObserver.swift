//
//  CORInAppPurchases+SKPaymentTransactionObserver.swift
//  CORInAppPurchases
//
//  Created by Siema on 18/11/2020.
//

import Foundation
import StoreKit

import CORCommon

extension CORInAppPurchases : SKPaymentTransactionObserver {
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        NSLog("InAppPurchases :: CORInAppPurchases.paymentQueue:updatedTransactions")
        
        if (!productsReady)
        {
            NSLog("products not yet ready, scheduling transactions for later processing")
            pendingTransactions.append(contentsOf: transactions)
            return
        }
        
        processTransactions(transactions)
    }
    
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        UnityProxyQueryPurchasesSucceeded()
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        UnityProxyQueryPurchasesFailed(error.localizedDescription)
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
        return true
    }
    
    internal func processTransactions(_ skTransactions: [SKPaymentTransaction])
    {
        let transactions = Transactions()
        
        skTransactions.forEach {skTransaction in
            NSLog("transactionState: \(Transaction.getStateString(skTransaction.transactionState)) "
                  + "productIdentifier: \(skTransaction.payment.productIdentifier)")
            
            
            let transaction = Transaction(skTransaction)
            if let identifier = skTransaction.transactionIdentifier {
                self.transactions[identifier] = transaction
            }
            transactions.append(transaction)
        }
        
        UnityProxyPurchasesUpdated(transactions.jsonString)
    }
}
