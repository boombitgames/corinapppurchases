//
//  CORInAppPurchases+SKProductsRequestDelegate.swift
//  CORInAppPurchases
//
//  Created by Siema on 18/11/2020.
//

import Foundation
import StoreKit

import CORCommon

extension CORInAppPurchases : SKProductsRequestDelegate {
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        guard handleInvalidProductIdentifiers(response) else { return }
        
        for skProduct in response.products {
            NSLog("InAppPurchases :: CORInAppPurchases.productsRequest:didReceive :: \(skProduct.productIdentifier)")
            let product = Product(skProduct)
            products[product.productIdentifier] = product
        }
        
        NSLog("InAppPurchases :: CORInAppPurchases.productsRequest:didReceive :: All products processed, sending to Unity")
        
        UnityProxyQueryInventorySucceeded(getProductsJson())
        productsReady = true
        resolvePendingTransactions()
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        UnityProxyQueryInventoryFailed(error.localizedDescription)
    }
    
    private func handleInvalidProductIdentifiers(_ response: SKProductsResponse) -> Bool {
        if (response.invalidProductIdentifiers.isEmpty) { return true }
        
        for identifier in response.invalidProductIdentifiers {
            NSLog("InAppPurchases :: CORInAppPurchases.handleInvalidProductIdentifiers :: \(identifier)")
        }
        
        if (response.products.isEmpty) {
            UnityProxyQueryInventoryFailed("No valid product identifiers")
            return false
        }
        
        return true
    }
    
    private func resolvePendingTransactions() {
        if (pendingTransactions.isEmpty) { return }
        
        NSLog("InAppPurchases :: CORInAppPurchases:resolvePendingTransactions")
        
        var transactionsToProcess: [SKPaymentTransaction] = []
        pendingTransactions.removeAll { (skTransaction) -> Bool in
            if let _ = products[skTransaction.payment.productIdentifier] {
                transactionsToProcess.append(skTransaction)
                return true
            }
            
            return false
        }
        if (!transactionsToProcess.isEmpty) { processTransactions(transactionsToProcess) }
    }
}
