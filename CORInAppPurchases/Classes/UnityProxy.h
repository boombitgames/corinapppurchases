//
//  UnityProxy.h
//  Pods
//
//  Created by Siema on 08/04/2021.
//

#pragma once

#include <CORCommon/CORCommon.h>

void UnityProxyQueryInventorySucceeded(const char* param);
void UnityProxyQueryInventoryFailed(const char* param);

void UnityProxyQueryPurchasesStarted(void);
void UnityProxyQueryPurchasesSucceeded(void);
void UnityProxyQueryPurchasesFailed(const char* param);

void UnityProxyPurchasesUpdated(const char* param);
