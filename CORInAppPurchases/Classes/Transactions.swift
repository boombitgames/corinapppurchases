//
//  Transactions.swift
//  CORInAppPurchases
//
//  Created by Siema on 17/03/2024.
//

import Foundation

class Transactions: Encodable {
    var transactions: [Transaction] = []
    
    init() {}
    
    init(_ newTransaction: Transaction) {
        transactions.append(newTransaction)
    }
    
    init(_ newTransactions: [Transaction]) {
        transactions.append(contentsOf: newTransactions)
    }
    
    func append(_ newTransaction: Transaction) {
        transactions.append(newTransaction)
    }
    
    func append(_ newTransactions: [Transaction]) {
        transactions.append(contentsOf: newTransactions)
    }
    
    var jsonString: String {
        do {
            let data = try CORInAppPurchases.shared.jsonEncoder.encode(self)
            if let jsonString = String(data: data, encoding: .utf8) { return jsonString }
            else { throw CORError("Couldn't convert encoded data to JSON String") }
        } catch {
            NSLog("InAppPurchases :: Transactions.jsonString :: Error encoding JSON: \(error)")
            return ""
        }
    }
}
