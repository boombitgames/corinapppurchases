//
//  UnityProxy.m
//  CORInAppPurchases
//
//  Created by Siema on 28/10/2020.
//

#include "UnityProxy.h"

#import <CORInAppPurchases/CORInAppPurchases-Swift.h>

StringDelegate _unityProxyQueryInventorySucceeded = NULL;
void UnityProxyQueryInventorySucceeded(const char* param)
{
    if (_unityProxyQueryInventorySucceeded != NULL) _unityProxyQueryInventorySucceeded(param);
}
StringDelegate _unityProxyQueryInventoryFailed = NULL;
void UnityProxyQueryInventoryFailed(const char* param)
{
    if (_unityProxyQueryInventoryFailed != NULL) _unityProxyQueryInventoryFailed(param);
}

BasicDelegate _unityProxyQueryPurchasesStarted = NULL;
void UnityProxyQueryPurchasesStarted(void)
{
    if (_unityProxyQueryPurchasesStarted != NULL) _unityProxyQueryPurchasesStarted();
}
BasicDelegate _unityProxyQueryPurchasesSucceeded = NULL;
void UnityProxyQueryPurchasesSucceeded(void)
{
    if (_unityProxyQueryPurchasesSucceeded != NULL) _unityProxyQueryPurchasesSucceeded();
}
StringDelegate _unityProxyQueryPurchasesFailed = NULL;
void UnityProxyQueryPurchasesFailed(const char* param)
{
    if (_unityProxyQueryPurchasesFailed != NULL) _unityProxyQueryPurchasesFailed(param);
}

StringDelegate _unityProxyPurchasesUpdated = NULL;
void UnityProxyPurchasesUpdated(const char* param)
{
    if (_unityProxyPurchasesUpdated != NULL) _unityProxyPurchasesUpdated(param);
}

FOUNDATION_EXPORT void _inAppPurchasesRegisterQueryInventorySucceededDelegate(StringDelegate func)
{
    _unityProxyQueryInventorySucceeded = func;
}

FOUNDATION_EXPORT void _inAppPurchasesRegisterQueryInventoryFailedDelegate(StringDelegate func)
{
    _unityProxyQueryInventoryFailed = func;
}

FOUNDATION_EXPORT void _inAppPurchasesRegisterQueryPurchasesStartedDelegate(BasicDelegate func)
{
    _unityProxyQueryPurchasesStarted = func;
}

FOUNDATION_EXPORT void _inAppPurchasesRegisterQueryPurchasesSucceededDelegate(BasicDelegate func)
{
    _unityProxyQueryPurchasesSucceeded = func;
}

FOUNDATION_EXPORT void _inAppPurchasesRegisterQueryPurchasesFailedDelegate(StringDelegate func)
{
    _unityProxyQueryPurchasesFailed = func;
}

FOUNDATION_EXPORT void _inAppPurchasesRegisterPurchasesUpdatedDelegate(StringDelegate func)
{
    _unityProxyPurchasesUpdated = func;
}

FOUNDATION_EXPORT void _inAppPurchasesRequestProducts(char* productIdentifiers)
{
    [[CORInAppPurchases shared] requestProducts:GetStringParam(productIdentifiers)];
}

FOUNDATION_EXPORT void _inAppPurchasesRestoreCompletedTransactions(void)
{
    [[CORInAppPurchases shared] restoreCompletedTransactions];
}

FOUNDATION_EXPORT bool _inAppPurchasesCanMakePayments(void)
{
    return [[CORInAppPurchases shared] canMakePayments];
}

FOUNDATION_EXPORT void _inAppPurchasesPurchaseProduct(char* productIdentifier)
{
    [[CORInAppPurchases shared] purchase:GetStringParam(productIdentifier)];
}

FOUNDATION_EXPORT void _inAppPurchasesFinishTransaction(char* transactionIdentifier)
{
    [[CORInAppPurchases shared] finishTransaction:GetStringParam(transactionIdentifier)];
}

FOUNDATION_EXPORT void _inAppPurchasesRefreshReceipt(void)
{
    [[CORInAppPurchases shared] refreshReceipt];
}

FOUNDATION_EXPORT char* _inAppPurchasesGetReceiptPath(void)
{
    return MakeStringCopy([[CORInAppPurchases shared] getReceiptPath]);
}

FOUNDATION_EXPORT char* _inAppPurchasesGetReceipt(void)
{
    return MakeStringCopy([[CORInAppPurchases shared] getReceipt]);
}

FOUNDATION_EXPORT char* _inAppPurchasesGetStoreCountryCode(void)
{
    return MakeStringCopy([[CORInAppPurchases shared] getStoreCountryCode]);
}
