//
//  CORExtensions.swift
//  CORCommon
//
//  Created by Siema on 30/11/2020.
//

import Foundation

public extension String { //MARK: - conversion to byte arrays
    func asInt8Pointer() -> UnsafePointer<Int8>? {
        return self.withCString { (pointer: UnsafePointer<Int8>) -> UnsafePointer<Int8>? in
            return pointer
        }
    }
    
    func asUInt8Pointer() -> UnsafePointer<UInt8>? {
        guard let data = self.data(using: String.Encoding.utf8) else { return nil }

        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: data.count)
        let stream = OutputStream(toBuffer: buffer, capacity: data.count)

        stream.open()
        data.withUnsafeBytes { (rawBufferPointer: UnsafeRawBufferPointer) -> Void in
            let boundPointer = rawBufferPointer.bindMemory(to: UInt8.self)
            stream.write(boundPointer.baseAddress!, maxLength: data.count)
        }
        stream.close()

        return UnsafePointer<UInt8>(buffer)
    }
}
