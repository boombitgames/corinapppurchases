//
//  CORUnityInterface.m
//  CORCommon
//
//  Created by Siema on 10/12/2020.
//

#include "dlfcn.h"

FOUNDATION_EXPORT void CORUnitySendMessage(const char* obj, const char* method, const char* msg)
{
    typedef void (*usm_t)(const char*, const char*, const char*);
    static usm_t usm_ptr = NULL;
    if (usm_ptr == NULL)
    {
        dlerror(); // clear errors
        usm_ptr = (usm_t)dlsym(RTLD_DEFAULT, "UnitySendMessage");
        const char* error = dlerror();
        if (error != NULL)
        {
            NSLog(@"Error when calling UnitySendMessage: %@", [NSString stringWithUTF8String:error]);
            return;
        }
    }
    
    usm_ptr(obj, method, msg);
}
