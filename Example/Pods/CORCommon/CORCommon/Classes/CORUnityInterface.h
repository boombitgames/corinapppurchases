//
//  CORUnityInterface.h
//  CORCommon
//
//  Created by Siema on 30/11/2020.
//

#pragma once

FOUNDATION_EXPORT void CORUnitySendMessage(const char* obj, const char* method, const char* msg);
