//
//  CORError.swift
//  CORCommon
//
//  Created by Siema on 20/11/2020.
//

import Foundation

public class CORError: Error {
    private let message: String

    public init(_ message: String) {
        self.message = message
    }

    public var localizedDescription: String {
        return message
    }
}
